$(document).ready(function () {
    if (localStorage.getItem('notification')) {
        $.notify({
            message: localStorage.getItem('notification')
        });
        localStorage.removeItem('notification');
    }
    $('#createTaskForm').on('submit', function () {
        $.ajax({
            data: $(this).serialize(),
            type: 'POST',
            url: '/task/create',
            success: function (result) {

                localStorage.setItem('notification', 'Задача добавлена');
                location.reload();
            }, error: function (result) {
                $('#createTaskModal').modal('hide');
                $.notify({
                    message: JSON.parse(result.responseText).error
                }, {
                    type: 'danger'
                });
                $('#createTaskForm input, textarea').each(function () {
                    $(this).val('');
                });
            }
        });
        return false;
    });

    $('.filter-button').on('click', function () {
        var action = $(this).data('action');
        var value = $(this).data('value');

        var $input = $('<input />')
            .attr('name', 'sort_' + action)
            .attr('type', 'hidden')
            .attr('value', (value == 0) ? 1 : 0);
        $input.val((value == 0) ? 1 : 0).attr('name', 'sort_' + action);
        $input.appendTo('#filterForm');
        $(this).siblings('.filter-button').each(function () {
            console.log($(this).data('value'));
            if ($(this).data('value') == 1) {
                var $input = $('<input />')
                    .attr('name', 'sort_' + $(this).data('action'))
                    .attr('type', 'hidden')
                    .attr('value', $(this).data('value'));
                $input.appendTo('#filterForm');
            }
        });
        $('#filterForm').submit();
    });

    $('#loginForm').on('submit', function () {
        $.ajax({
            data: $(this).serialize(),
            type: 'POST',
            url: '/user/login',
            success: function (result) {
                // console.log(result);
                location.reload();

            }, error: function (response) {
                console.log(response);
                $.notify({
                    message: JSON.parse(response.responseText).error
                }, {
                    type: 'danger',
                    z_index: 2000,
                })
            }
        });
        return false;
    });
    $('.update-button').on('click', function () {
        $.ajax({
            data: {
                id: $(this).data('value')
            },
            type: 'GET',
            url: '/task/get',
            success: function (result) {
                var $form = $('#updateTaskForm');
                $form.find('textarea[name=content]').val(result['content']);
                $form.find('input[name=id]').val(result['id']);
                if (result['status'] == 1) {
                    $form.find('.status-check').html('');
                    $form.find('.status-check').append('<input type="checkbox" name="edited" value="1">Выполнено')
                }
                $('#updateTaskModal').modal('show');
            }
        });
    });
    $('#updateTaskForm').on('submit', function () {
        $.ajax({
            data: $(this).serialize(),
            type: 'POST',
            url: '/task/update',
            success: function () {
                localStorage.setItem('notification', 'Задача обновлена');
                location.reload();
            }, error: function (response) {
                $('#updateTaskModal').modal('hide');
                $.notify({
                    message: JSON.parse(response.responseText).error
                }, {
                    type: 'danger'
                })
            }
        });
        return false;
    });
    $('#logoutButton').on('click', function () {
        $.ajax({
            data: $(this).serialize(),
            type: 'GET',
            url: '/logout',
            success: function () {
                location.reload();
            }, error: function (response) {
                $.notify({
                    message: JSON.parse(response.responseText).error
                }, {
                    type: 'danger'
                })
            }
        });
        return false;
    })
});
