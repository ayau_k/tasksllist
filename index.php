<?php
/**
 * Created by PhpStorm.
 * User: ayauzhan
 * Date: 2020-10-04
 * Time: 21:55
 */
ini_set('display_errors', 1);
spl_autoload_register(function($class) {

    $a = explode('\\', $class);
    $fn =   $class . '.php';
    $fn = str_replace('\\', '/', $fn);
    if (file_exists($fn)) {
        require $fn;

    }
});

require_once 'app/import.php';