<div class="row mt-3">
    <div class="col-4">
        На странице отображен список задач, добавленных пользователями.
        <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#createTaskModal">
            Добавить задачу
        </button>
    </div>
    <div class="col">
        <div class="tasks-list">
            <div class="filters mb-3">
                Сортировать:
                <form id="filterForm"
                      action="/tasks<?= ($data['current_page'] > 1) ? '/' . $data['current_page'] : '' ?>" method="get">
                    <button type="button" class="btn btn-outline-secondary filter-button py-1 px-2" data-action="name"
                            data-value="<?= $data['sort_name'] ?>">Имя
                        <span>
                        <?= ($data['sort_name'] == 0) ? "&#8595;" : "&#8593;" ?>
                    </span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary filter-button py-1 px-2" data-action="email"
                            data-value="<?= $data['sort_email'] ?>">email
                        <span>
                        <?= ($data['sort_email'] == 0) ? "&#8595;" : "&#8593;" ?>
                    </span>
                    </button>
                    <button type="button" class="btn btn-outline-secondary filter-button py-1 px-2" data-action="status"
                            data-value="<?= $data['sort_status'] ?>">Статус
                        <span>
                        <?= ($data['sort_status'] == 0) ? "&#8595;" : "&#8593;" ?>
                    </span>
                    </button>
                </form>
            </div>
            <?php
            foreach ($data['tasks'] as $d) { ?>
            <div class="card mb-3 p-0">
                <div class="card-header">
                    <div class="float-left">
                        <?= $d['username'] ?>
                    </div>
                    <div class="float-right">
                            <span class="badge badge-info">
                                <?= $d['status_name'] ?>
                            </span>
                        <?php if ($d['is_edited']) { ?>
                        <span class="badge badge-secondary">
                                Отредактировано администратором
                            </span>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= $d['content'] ?></p>
                </div>
                <?php if (isset($_SESSION['token']) && $_SESSION['token']) { ?>
                    <div class="card-footer">
                        <button class="btn btn-link update-button" data-value="<?= $d['id'] ?>">Редактировать</button>
                    </div>
                <?php } ?>
            </div>
        <? } ?>
        </div>
        <?php if ($data['number_pages'] > 1) { ?>
            <nav aria-label="">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link"
                           href="/tasks<?= ($data['previous_page']) ? '/' . $data['previous_page'] : '' ?><?= $data['sort_url'] ?>">Previous</a>
                    </li>
                    <?php
                    for ($i = 1; $i <= $data['number_pages']; $i++) {
                        if ($data['current_page'] == $i) { ?>
                            <li class="page-item active">
                    <span class="page-link">
                        <?= $i ?>
                    <span class="sr-only">(current)</span>
                        </span>
                            </li>
                        <?php } else { ?>
                            <li class="page-item numbered-page" data-number="<?= $i ?>">
                                <a class="page-link"
                                   href="/tasks<?= ($i == 1) ? '' : '/' . $i ?><?= $data['sort_url'] ?>"> <?= $i ?></a>
                            </li>
                        <?php } ?>

                    <?php }
                    ?>
                    <li class="page-item"><a class="page-link"
                                             href="/tasks/<?= $data['next_page'] . $data['sort_url'] ?>">Next</a></li>
                </ul>
            </nav>

        <?php } ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="createTaskModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Новое задание</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/task/create" method="POST" id="createTaskForm">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" name="email"
                               aria-describedby="emailHelp" required>
                    </div>
                    <div class="form-group">
                        <label for="username">Имя</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleCheck1">Задача</label>
                        <textarea class="form-control" name="content" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateTaskModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редактировать</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/task/update" method="POST" id="updateTaskForm">
                    <div class="form-group">
                        <label>Задача</label>
                        <textarea rows="8" class="form-control" name="content" required></textarea>
                    </div>
                    <input type="hidden" name="id" value="">
                    <div class="status-check my-2">
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>