<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <script src="/public/jquery-3.3.1.js" type="text/javascript"></script>
    <link type="text/css" href="/public/bootstrap-4.5.2-dist/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/public/styles/style.css" rel="stylesheet">
    <script src="/public/bootstrap-4.5.2-dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/public/scripts/script.js" type="text/javascript"></script>
    <script src="/public/bootstrap-notify-3.1.3/bootstrap-notify.min.js" type="text/javascript"></script>
</head>
<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Tasks</h5>
    <?php if (isset($_SESSION['token']) && $_SESSION['name']) { ?>
        <?= $_SESSION['name'] ?>
        <button type="button" id="logoutButton" class="ml-4 btn btn-outline-primary">Выйти</button>
    <?php } else { ?>
        <button id="loginButton" class="btn btn-outline-primary" data-toggle="modal" data-target="#loginModal">Войти
        </button>
    <?php } ?>
</div>

<!-- Begin page content -->
<main class="container">
    <?php include 'templates/' . $content; ?>
</main>
<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Войти</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="loginForm" action="/user/login" method="post">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Логин</label>
                        <input class="form-control" name="login" type="text" placeholder="Логин" required>
                    </div>
                    <div>
                        <label>Пароль</label>
                        <input class="form-control" name="password" type="password" placeholder="Пароль" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>