<?php

//require 'app/Models/Task.php';
namespace app\Controllers;

use app\Models\Task as Task;

class TasksController extends Controller
{
    public function index($page)
    {
        $currentPage = (isset($page[0]) && $page[0]) ? intval($page[0]) : 1;
        $tasksPerPage = 3;
        $offset = ($currentPage - 1) * $tasksPerPage;
        $previousPage = $currentPage - 1;
        $nextPage = $currentPage + 1;
        $tasks = new Task();
        $numberOfTasks = $tasks->count();
        $numberOfPages = ceil($numberOfTasks / $tasksPerPage);
        $retrievedTasks = $tasks->select(['tasks.id as id', 'username', 'content','tasks.status',  'task_statuses.name as status_name', 'email', 'is_edited'])
            ->limit($tasksPerPage)
            ->offset($offset)
            ->withStatus();
//
        $sortUrlParams = [];
        $sortByName = $sortByEmail = $sortByStatus = 0;
        if (isset($_GET['sort_name'])) {
            $sortByName = $_GET['sort_name'];
            $retrievedTasks = $retrievedTasks->sortBy('username', ($sortByName) ? 'ASC' : 'DESC');
            $sortUrlParams[] = 'sort_name=' . $sortByName;
        }
        if (isset($_GET['sort_email'])) {
            $sortByEmail = $_GET['sort_email'];
            $retrievedTasks = $retrievedTasks->sortBy('email', ($sortByEmail) ? 'ASC' : 'DESC');
            $sortUrlParams[] = 'sort_email=' . $_GET['sort_email'];
        }
        if (isset($_GET['sort_status'])) {
            $sortByStatus =$_GET['sort_status'];
            $retrievedTasks = $retrievedTasks->sortBy('tasks.status', ($sortByStatus) ? 'ASC' : 'DESC');
            $sortUrlParams[] = 'sort_status=' . $sortByStatus;
        }

        $retrievedTasks = $retrievedTasks->get();
        $sortUrl = '?';
        foreach ($sortUrlParams as $param) {
            $sortUrl .= $param . '&';
        }

        $data = [
            'tasks' => $retrievedTasks,
            'current_page' => $currentPage,
            'next_page' => $nextPage,
            'previous_page' => $previousPage,
            'number_pages' => intval($numberOfPages),
            'sort_name' => intval($sortByName),
            'sort_email' => intval($sortByEmail),
            'sort_status' => intval($sortByStatus),
            'sort_url' => rtrim($sortUrl, '&')
        ];
        header('Content-Type: text/html');
        $this->view->generate('tasks.php', 'general.php', $data);
    }


    public function getTask()
    {
        $tasks = new Task();
        $tasks->where('id', '=', $_GET['id']);
        $retrieved = $tasks->get();
        header('Content-Type: application/json');
        echo json_encode($retrieved[0]);
    }

    public function createTask()
    {
        try {
            $inputData = $this->validate($_POST, ['username', 'email', 'content']);
        } catch (Exception $e) {
            $this->errorResponse(500, $e->getMessage());
        }
        if(!filter_var($inputData['email'], FILTER_VALIDATE_EMAIL))
            throw new Exception('Некорректный адрес электоронной почты');
        $task = new Task();
        $task->setAttributes($inputData);
        $id = $task->create();

        $savedTask = $task->where('id', '=', $id)
            ->get();
        header('Content-Type: application/json');
        echo json_encode(['task' => $savedTask[0]]);
        exit;
    }

    public function updateTask()
    {
        if(!isset($_SESSION['token'])) {
            $this->errorResponse(403, 'Недостаточно прав');
        }
        if (!isset($_POST['id'])) {
            $this->errorResponse(500, 'Запись не найдена');
        }
        $data = $this->validate($_POST, ['content']);
        if (isset($_POST['edited'])){
            $data['is_edited'] = 1;
            $data['status'] = 2;
        }
        $task = new Task();
        $task->setAttributes($data)
            ->where('id', '=', $_POST['id'])
            ->update();
    }
}