<?php

//require 'app/Models/User.php';
namespace app\Controllers;

use app\Models\User as User;

class UserController extends Controller
{
    public function login()
    {
        $data = $this->validate($_POST, ['login', 'password']);
        $user = new User();
        try {
            $loginData = $user->login($data['login'], $data['password']);
        } catch (\Exception $exception) {
            $this->errorResponse(403, $exception->getMessage());
        }
        foreach ($loginData as $key => $item) {
            $_SESSION[$key] = $item;
        }
        header('Content-Type: application/json');
        echo json_encode($loginData);
        exit;
    }

    public function create()
    {
        $user = new User();
        $user->setAttributes([
            'login' => $_POST['login'],
            'password' => $_POST['password'],
            'name' => $_POST['name']
        ]);
        $user->create();
    }

    public function logout()
    {
        session_destroy();
    }
}