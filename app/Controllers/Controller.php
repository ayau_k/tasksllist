<?php
namespace app;
require 'app/Views/View.php';

namespace app\Controllers;

use app\Views\View as View;
use Exception;

class Controller
{
    public $view;

    function __construct()
    {
        $this->view = new View();
        session_start();
    }

    function action_index()
    {
    }

    protected function validate(array $inputData, array $attributes)
    {
        $sortedInputData = array();
        foreach ($attributes as $attribute) {
            if (!key_exists($attribute, $inputData) || empty($inputData[$attribute]) )
                throw new Exception('Параметр ' . $attribute . ' обязателен');
            $sortedInputData[$attribute] = (is_string($inputData[$attribute])) ? strip_tags($inputData[$attribute]) : $inputData[$attribute];
        }
        return $sortedInputData;
    }

    public function errorResponse($code, $message)
    {
        header('Content-Type: application/json');
        http_response_code($code);
        echo json_encode(['error' => $message]);
        exit;
    }
}