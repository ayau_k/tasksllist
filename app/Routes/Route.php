<?php
namespace app\Routes;
//namespace app;

class Route
{
//    use app\Controllers\TasksController as TaskController;

    protected static  function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }

    private static $routes = array(
        '/tasks[/]{0,1}(\d*)?.*' => 'app\Controllers\TasksController@index',
        '/task/create' => 'app\Controllers\TasksController@createTask',
        '/task/update' => 'app\Controllers\TasksController@updateTask',
        '/task/get?.*' => 'app\Controllers\TasksController@getTask',
        '/user/create' => 'app\Controllers\UserController@create',
        '/user/login' => 'app\Controllers\UserController@login',
        '/logout' => 'app\Controllers\UserController@logout'
    );

    private function __construct() {}
    private function __clone() {}


    public static function execute($url)
    {
        foreach (self::$routes as $pattern => $callback)
        {
            $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';
            if (preg_match($pattern, $url, $params)) // сравнение идет через регулярное выражение
            {
                array_shift($params);
                list($controller, $action) = explode('@', $callback);
//                $controller = "Controllers\\" .$controller;
                $controller = new $controller;

                if(method_exists($controller, $action)) {
                    $controller->$action((isset($params)) ? $params : []);
                } else {
                    Route::ErrorPage404();
                }

                exit();
            }

        }
    }

}