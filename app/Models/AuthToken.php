<?php
namespace app\Models;

use DateInterval;
use DateTime;

class AuthToken extends Model
{
    protected $tableName = 'auth_tokens';

    public static function createToken($user_id)
    {
        $currentDate = date('Y-m-d H:i:s');
        $currentDateTime = new DateTime($currentDate);
        $currentDateTime->add(new DateInterval('PT20M'));
        $token = md5($currentDate);

        $authToken = new self();
        $authToken->setAttributes([
            'user_id' => $user_id,
            'token' => $token,
            'expires_at' => $currentDateTime->format('Y-m-d H:i:s')
        ]);
        $authTokenId = $authToken->create();

        $retrievedToken = $authToken->select(['token'])
            ->where('id', '=', $authTokenId)
            ->get();
        return $retrievedToken[0]['token'];
    }

}