<?php
namespace app\Models;
class Task extends Model
{
    protected $tableName = 'tasks';
    public $attributes;

    public function withStatus()
    {
        return $this->join('task_statuses', 'status', 'id');
    }

    public function count()
    {
        $this->selectColumns = 'COUNT(*)';
        $result = $this->get();
        return $result[0]['COUNT(*)'];
    }

    public static function countAll()
    {
        $tasks = new self();
        $tasks->selectColumns = 'COUNT(*)';
        $result = $tasks->get();
        return $result[0]['COUNT(*)'];
    }
}