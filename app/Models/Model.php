<?php
namespace app\Models;
use PDO;
use PDOException;

class Model
{

    /**
     * @var PDO
     */
    protected $db;
    /**
     * @var string
     */
    protected $tableName;

    protected $attributes;

    protected $selectColumns;

    protected $whereConditions = [];

    protected $sortColumns;

    protected $sortConditions = [];

    protected $join = [];

    protected $offset;

    protected $limit;

    public function __construct()
    {
        $this->connect();
    }

    protected function connect()
    {
        try {
            $this->db = new PDO('mysql:dbname=beejee;host=localhost',
                'root',
                '',
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
            );
        } catch (PDOException $e) {
        }
    }

    public function get()
    {
        $statement = "SELECT ". (($this->selectColumns) ? $this->selectColumns : '*')  . " FROM " . $this->tableName . " ";
        $whereClosure = '';
        $values = [];
        foreach ($this->whereConditions as $key => $condition) {
            $whereClosure .= $condition['column']  . $condition['action'] . ':' . $condition['column'];
            $values[$condition['column']] = $condition['value'];
            if ($key != count($this->whereConditions) - 1)
                $whereClosure .= ' AND';
        }
        $statement .= ($whereClosure) ? ' WHERE ' . rtrim($whereClosure, ',') : '';

            $joinClosure = '';
            foreach ($this->join as $join) {
                $joinClosure .= ' LEFT JOIN ' . $join['table'] . ' ON ' . $join['foreignKey'] . '=' . $join['joinKey'];
            }
            $statement .= $joinClosure;

        if ($this->sortConditions) {
            $sort = ' ORDER BY ';

            foreach ($this->sortConditions as $condition) {
                $sort .= $condition['column'] . ' ' . $condition['type'] . ',';
            }
            $statement .=  rtrim($sort, ',');
        }
        $statement .= ($this->limit) ? ' LIMIT ' . $this->limit : '';
        $statement .= ($this->offset) ? ' OFFSET ' . $this->offset : '';
        $preparedStatement = $this->db->prepare($statement);
        $preparedStatement->execute($values);
        $fetchedData = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        return $fetchedData;
    }

    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function create()
    {
        $setParameters = '';
        foreach (array_keys($this->attributes) as $key) {
            $setParameters .= $key. '=:' . $key . ',';
        }
        $statement = $this->db->prepare('INSERT INTO '. $this->tableName .' SET ' . rtrim($setParameters, ','));
        $statement->execute($this->attributes);
        return $this->db->lastInsertId();
    }

    public function update()
    {
        $setParameters = '';
        foreach (array_keys($this->attributes) as $key) {
            $setParameters .= $key. '=:' . $key . ',';
        }

        $whereClosure = '';
        $values = [];
        foreach ($this->whereConditions as $key => $condition) {
            $whereClosure .= $condition['column']  . $condition['action'] . ':' . $condition['column'];
            $values[$condition['column']] = $condition['value'];
            if ($key != count($this->whereConditions) - 1)
                $whereClosure .= ' AND';
        }
        $whereClosure = ($whereClosure) ? ' WHERE ' . rtrim($whereClosure, ',') : '';
        $statement = $this->db->prepare('UPDATE '. $this->tableName .' SET ' . rtrim($setParameters, ',') . $whereClosure);
        $statement->execute(array_merge($this->attributes, $values));
    }

    public function select(array $columnsToRetrieve)
    {
        $selectColumns = '';
        foreach ($columnsToRetrieve as $columnName) {
            $selectColumns .= $columnName . ',';
        }
        $this->selectColumns = ($selectColumns) ? rtrim($selectColumns, ', ') : '*';
        return $this;
    }

    public function where($column, $action, $value)
    {
        $this->whereConditions[] = ['column' => $column, 'action' => $action, 'value' => $value];
        return $this;
    }

    public function sortBy($column, $type)
    {
        $this->sortConditions[] = ['column' => $column, 'type' => $type];
        return $this;
    }

    public function join($table, $foreignKey, $joinKey)
    {
        $this->join[] = ['table' => $table, 'foreignKey' => $this->tableName . '.' . $foreignKey, 'joinKey' => $table . '.' .$joinKey];
        return $this;
    }

    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }
}