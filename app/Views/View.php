<?php
namespace app\Views;

class View
{

    function generate($content, $template, $data = null)
    {
        include 'templates/' . $template;
    }
}